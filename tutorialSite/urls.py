"""tutorialSite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from lectures.views import show_lectures_list, show_lecture
from section.views import show_sections_list
from tutorialSite import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', show_sections_list, name='show_sections_list'),
    path('lectures-list/<int:section_id>/', show_lectures_list, name='show_lectures_list'),
    path('lecture/<int:lecture_id>/', show_lecture, name='show_lecture'),

    # all-auth urls. Social log in off, for a moment
    path('accounts/', include('allauth.urls')),

    path('api/', include('section.api_urls'), name='all_section_api'),
    path('api/', include('lectures.api_urls')),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + \
              static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
