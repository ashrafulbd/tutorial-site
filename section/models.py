from django.db import models

from django.db.models import Sum

from lectures.models import Lecture
from users.models import User


class Section(models.Model):
    title = models.CharField(max_length=100, blank=False, null=False)
    description = models.TextField(blank=False, null=False)
    lectures = models.ManyToManyField(Lecture, related_name='followed_section', blank=True)
    visibility = models.BooleanField(default=True)

    @property
    def total_duration(self):
        return self.lectures.aggregate(Sum('duration')).get('duration__sum')

    @property
    def total_lecture(self):
        return self.lectures.count()

    @property
    def get_total_likes(self):
        return SectionLike.objects.filter(section=self.id, is_liked=True).count()

    @property
    def get_total_dislikes(self):
        return SectionLike.objects.filter(section=self.id, is_liked=False).count()

    @property
    def get_all_comments(self):
        return SectionComment.objects.filter(section=self.id)

    def __str__(self):
        return self.title


class SectionLike(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    section = models.ForeignKey(Section, on_delete=models.CASCADE)
    is_liked = models.BooleanField(blank=False, null=True)

    class Meta:
        unique_together = ('user', 'section')

    def update_like_status(self, bool_value):
        if self.is_liked == bool_value:
            self.is_liked = None
        self.is_liked = bool_value
        self.save()

    def __str__(self):
        return f'{self.user.email} > {self.is_liked} > {self.section.title}'


class SectionComment(models.Model):
    createdAt = models.DateTimeField(auto_now_add=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    section = models.ForeignKey(Section, on_delete=models.CASCADE)
    comment = models.TextField(blank=False, null=False)

    def __str__(self):
        return f'{self.user.email} > {self.comment} > {self.section.title}'
