from django.contrib import admin

# Register your models here.
from section.models import Section, SectionLike, SectionComment

admin.site.register(Section)
admin.site.register(SectionLike)
admin.site.register(SectionComment)
