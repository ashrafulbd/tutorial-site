from django.shortcuts import render

from section.models import Section


def show_sections_list(request):
    context = {
        'sections': Section.objects.all().order_by('title')
    }
    return render(request, "home.html", context)





