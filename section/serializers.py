from rest_framework import serializers

from section.models import Section, SectionComment, SectionLike


class SectionCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = SectionComment
        fields = '__all__'


class SectionLikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SectionLike
        fields = '__all__'


class SectionViewSerializer(serializers.ModelSerializer):
    get_all_comments = SectionCommentSerializer(many=True, read_only=True)

    class Meta:
        model = Section
        fields = ('id', 'title', 'description', 'visibility', 'total_duration', 'total_lecture',
                  'get_total_likes', 'get_total_dislikes', 'get_all_comments')

