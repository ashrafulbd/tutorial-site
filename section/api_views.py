from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_201_CREATED
from rest_framework.views import APIView

from section.models import Section, SectionLike
from section.serializers import SectionViewSerializer, SectionCommentSerializer, SectionLikeSerializer


class SectionViewSet(viewsets.ModelViewSet):
    serializer_class = SectionViewSerializer
    queryset = Section.objects.all()
    lookup_field = 'id'
    http_method_names = 'get'


class SectionLikeAPIView(APIView):

    # permission_classes = [HasGroupPermission]
    # required_groups = {
    #     'POST': ['Admin User']
    # }

    def post(self, request):
        if (user := request.data.get('user')) and \
                (section := request.data.get('section')) and \
                (is_liked := request.data.get('is_liked')):

            section_like_obj_list = SectionLike.objects.filter(user_id=user, section_id=section)
            if section_like_obj_list:
                if str(is_liked) == str(section_like_obj_list[0].is_liked):
                    is_liked = None
                section_like_obj_list[0].is_liked = is_liked
                section_like_obj_list[0].save()
                return Response(SectionLikeSerializer(section_like_obj_list[0]).data, status=HTTP_201_CREATED)

        serializer = SectionLikeSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=HTTP_201_CREATED)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class SectionCommentAPIView(APIView):

    # permission_classes = [HasGroupPermission]
    # required_groups = {
    #     'POST': ['Admin User']
    # }

    def post(self, request):
        serializer = SectionCommentSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=HTTP_201_CREATED)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

