from django.urls import path

from section.api_views import SectionViewSet, SectionLikeAPIView, SectionCommentAPIView


urlpatterns = [
    path('sections/', SectionViewSet.as_view({'get': 'list'}), name='api_sections'),

    path('section-like/', SectionLikeAPIView.as_view(), name='api_section_like'),
    path('section-comment/', SectionCommentAPIView.as_view(), name='api_section_comment'),
]
