import random

import factory
from factory import Faker
from factory import SubFactory
from factory.django import DjangoModelFactory

from lectures.test.factories import LectureFactory
from section.models import Section, SectionComment, SectionLike
from users.test.factories import UserFactory


class SectionFactory(DjangoModelFactory):
    title = Faker('name')
    description = Faker('text')
    lectures = SubFactory(LectureFactory)
    visibility = bool(random.getrandbits(1))

    class Meta:
        model = Section

    @factory.post_generation
    def lectures(self, create, extracted, **kwargs):
        if extracted:
            for lecture in extracted:
                self.lectures.add(lecture)
            return
        if create and not self.lectures.all():
            lecture = LectureFactory()
            self.lectures.add(lecture)


class SectionCommentFactory(DjangoModelFactory):
    user = SubFactory(UserFactory)
    section = SubFactory(SectionFactory)
    comment = Faker('text')

    class Meta:
        model = SectionComment


class SectionLikeFactory(DjangoModelFactory):
    user = SubFactory(UserFactory)
    section = SubFactory(SectionFactory)
    is_liked = random.choice((True, False))

    class Meta:
        model = SectionLike
