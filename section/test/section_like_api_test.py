import random

import pytest
from rest_framework.reverse import reverse

from section.models import SectionLike
from section.test.factories import SectionFactory, SectionLikeFactory


class Base:
    @pytest.fixture()
    def url(self):
        return reverse("api_section_like")

    @pytest.fixture()
    def section(self):
        return SectionFactory()


class TestSectionCommentAPI(Base):
    def test_section_like(self, user, section, url, client):
        data = {
            'is_liked': random.choice([True, False]),
            'user': user.id,
            'section': section.id
        }
        response = client.post(url, data=data)

        assert response.status_code == 201
        assert response.data.get('section') == section.id
        assert (section_like_obj := SectionLike.objects.get(id=response.data.get('id')))
        assert str(section_like_obj.is_liked) == str(data.get('is_liked'))

    def test_section_like_update(self, section, url, client):
        new_like = SectionLikeFactory()
        print(new_like)
        data = {
            'is_liked': new_like.is_liked,
            'user': new_like.user.id,
            'section': new_like.section.id
        }
        response = client.post(url, data=data)
        assert response.status_code == 201
        assert response.data.get('is_liked') is None
        response = client.post(url, data=data)
        assert str(response.data.get('is_liked')) == str(data.get('is_liked'))
