import pytest

from lectures.test.factories import LectureFactory
from section.test.factories import SectionFactory


class BaseSection:

    @pytest.fixture
    def section(self):
        return SectionFactory


@pytest.mark.django_db
class TestSection(BaseSection):

    def test_section(self, section):
        assert section
        assert section.lectures
