import pytest
from django.urls import reverse
from factory import create_batch

from lectures.test.factories import LectureFactory
from section.test.factories import SectionFactory


class Base:
    @pytest.fixture()
    def lectures(self):
        return create_batch(klass=LectureFactory, size=5)

    @pytest.fixture()
    def section(self, lectures):
        return SectionFactory(lectures=lectures)

    @pytest.fixture()
    def url(self):
        return reverse("api_sections")

    @pytest.fixture()
    def response(self, client, url, section, lectures):
        return client.get(url)


@pytest.mark.django_db(transaction=True)
class TestSectionAPIs(Base):
    def test_sections_list(self, response, lectures, section):
        assert response.status_code == 200
        assert response.data[0].get('title') == section.title
        assert response.data[0].get('total_lecture') == len(lectures)
        assert response.data[0].get('total_duration') == section.total_duration
