import pytest
from factory import Faker
from rest_framework.reverse import reverse

from section.test.factories import SectionFactory


class Base:

    @pytest.fixture()
    def url(self):
        return reverse("api_section_comment")

    @pytest.fixture()
    def section(self):
        return SectionFactory()


class TestSectionCommentAPI(Base):

    def test_section_comment(self, user, section, url, client):
        data = {
            'comment': Faker('text').generate(),
            'user': user.id,
            'section': section.id
        }
        response = client.post(url, data=data)

        assert response.status_code == 201
        assert response.data.get('section') == section.id
