import pytest
from rest_framework.test import APIClient

from users.test.factories import UserFactory


@pytest.fixture(autouse=True)
def enable_db_access(db):
    pass


@pytest.fixture
def client():
    return APIClient()


@pytest.fixture
def user():
    return UserFactory()


# @pytest.fixture
# def admin(user):
#     admin_user_group = GroupFactory(name="Admin User")
#     user.groups.add(admin_user_group)
#     return user


# @pytest.fixture
# def doctor(user):
#     doctor_user_group = GroupFactory(name="Doctor")
#     user.groups.add(doctor_user_group)
#     return user
#
#
# @pytest.fixture
# def nurse(user):
#     nurse_user_group = GroupFactory(name="Nurse")
#     user.groups.add(nurse_user_group)
#     return user


@pytest.fixture
def auth_user_client(user, client):
    client.force_authenticate(user)
    return client


# @pytest.fixture
# def auth_admin_client(admin, client):
#     client.force_authenticate(admin)
#     return client


