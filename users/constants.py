
MALE = 1
FEMALE = 2
Non_Binary = 3
NOT_TO_DISCLOSE = 4

SEX_CHOICES = (
    (MALE, "MALE"),
    (FEMALE, "FEMALE"),
    (Non_Binary, "Non-Binary"),
    (NOT_TO_DISCLOSE, "Prefer not to disclose"),
)
