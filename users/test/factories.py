from django.contrib.auth.models import Group
from factory import Faker
from factory.django import DjangoModelFactory

from users.models import User


class UserFactory(DjangoModelFactory):

    email = Faker('email')
    name = Faker('name')

    class Meta:
        model = User


class GroupFactory(DjangoModelFactory):
    name = Faker('word').generate()

    class Meta:
        model = Group

