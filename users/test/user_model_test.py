import pytest
from django.db import IntegrityError

from users.test.factories import UserFactory


class Base:
    @pytest.fixture
    def user(self):
        return UserFactory


class TestUser(Base):

    def test_user_unique_email(self, user):
        assert user
