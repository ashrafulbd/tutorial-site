
from rest_framework import serializers

from lectures.models import Lecture, LectureComment, Image


class LectureCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = LectureComment
        fields = '__all__'


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = '__all__'


class LectureListViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lecture
        fields = ('id', 'lecture_serial', 'title', 'duration')


class LectureDetailsViewSerializer(serializers.ModelSerializer):
    get_all_comments = LectureCommentSerializer(many=True, read_only=True)
    images = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = Lecture
        fields = ('id', 'createdAt', 'lecture_serial', 'title', 'description', 'images', 'duration', 'next_lecture',
                  'previous_lecture', 'get_all_comments')
