from django.shortcuts import render, get_object_or_404

from lectures.models import Lecture
from section.models import Section


def show_lectures_list(request, section_id):
    section_obj = get_object_or_404(Section, id=section_id)
    context = {
        'lectures': section_obj.lectures.all().order_by('lecture_serial')
    }
    return render(request, "lectures.html", context)


def show_lecture(request, lecture_id):
    lecture_obj = get_object_or_404(Lecture, id=lecture_id)
    context = {
        'lecture': lecture_obj
    }
    return render(request, "lecture.html", context)

