from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_200_OK, HTTP_201_CREATED
from rest_framework.views import APIView

from lectures.models import LectureComment
from lectures.serializers import LectureListViewSerializer, LectureDetailsViewSerializer, LectureCommentSerializer
from section.models import Section


class LectureViewSet(viewsets.ModelViewSet):
    lookup_field = 'id'
    http_method_names = 'get'

    def __init__(self, *args, **kwargs):
        super(LectureViewSet, self).__init__(*args, **kwargs)
        self.serializer_action_classes = {
            'list': LectureListViewSerializer,
            'retrieve': LectureDetailsViewSerializer
        }

    def get_serializer_class(self, *args, **kwargs):
        """Instantiate the list of serializers per action from class attribute (must be defined)."""
        kwargs['partial'] = True
        try:
            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return super(LectureViewSet, self).get_serializer_class()

    def get_queryset(self):
        section_id = self.kwargs.get('section_id')
        section_obj = get_object_or_404(Section, id=section_id)
        return section_obj.lectures.all().order_by('lecture_serial')


class LectureCommentAPIView(APIView):

    # permission_classes = [HasGroupPermission]
    # required_groups = {
    #     'POST': ['Admin User']
    # }

    def post(self, request):
        """
        create
        :param request:
        :return:
        """
        serializer = LectureCommentSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=HTTP_201_CREATED)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)