from django.urls import path, include
from rest_framework.routers import SimpleRouter

from lectures.api_views import LectureViewSet, LectureCommentAPIView


urlpatterns = [
    path('lectures/<int:section_id>/', LectureViewSet.as_view({'get': 'list'}), name='api_lectures_list'),
    path('lectures/<int:section_id>/<int:id>/', LectureViewSet.as_view({'get': 'retrieve'}), name='api_lectures_details'),

    path('lecture-comment/', LectureCommentAPIView.as_view(), name='api_lecture_comment'),
]