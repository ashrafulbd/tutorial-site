import pytest
from factory import Faker
from rest_framework.reverse import reverse

from lectures.test.factories import LectureFactory


class Base:
    @pytest.fixture()
    def lecture(self):
        return LectureFactory()

    @pytest.fixture()
    def url(self):
        return reverse("api_lecture_comment")


class TestLectureCommentAPI(Base):

    def test_lecture_comment(self, user, lecture, url, client):
        data = {
            'comment': Faker('text').generate(),
            'user': user.id,
            'lecture': lecture.id
        }
        response = client.post(url, data=data)

        assert response.status_code == 201
        assert response.data.get('lecture') == lecture.id
