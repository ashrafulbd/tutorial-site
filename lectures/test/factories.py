import factory
from factory import Faker
from factory import SubFactory
from factory.django import DjangoModelFactory
from factory.fuzzy import FuzzyInteger

from lectures.models import Image, Lecture, LectureComment
from users.test.factories import UserFactory


class ImageFactory(DjangoModelFactory):
    class Meta:
        model = Image


class LectureFactory(DjangoModelFactory):
    lecture_serial = FuzzyInteger(low=1, high=1000)
    title = Faker('name')
    description = Faker('text')
    duration = FuzzyInteger(low=1, high=1000)
    images = SubFactory(ImageFactory)

    @factory.post_generation
    def images(self, create, extracted, **kwargs):
        image = ImageFactory()
        self.images.add(image)
        return image

    class Meta:
        model = Lecture


class LectureCommentFactory(DjangoModelFactory):
    user = SubFactory(UserFactory)
    lecture = SubFactory(LectureFactory)
    comment = Faker('text')

    class Meta:
        model = LectureComment
