import pytest
from django.urls import reverse
from factory import create_batch

from lectures.test.factories import LectureFactory, LectureCommentFactory
from section.test.factories import SectionFactory


class Base:

    @pytest.fixture()
    def lectures(self):
        return create_batch(klass=LectureFactory, size=5)

    @pytest.fixture()
    def lecture_comments(self, lectures):
        return create_batch(klass=LectureCommentFactory, size=5, lecture=lectures[0])

    @pytest.fixture()
    def section(self, lectures):
        return SectionFactory(lectures=lectures)

    @pytest.fixture()
    def list_url(self, section):
        return reverse("api_lectures_list", args=[section.id])

    @pytest.fixture()
    def detail_url(self, section, lectures):
        return reverse("api_lectures_details", kwargs={'section_id': section.id, 'id': lectures[0].id})

    @pytest.fixture()
    def list_response(self, client, list_url, section, lectures, lecture_comments):
        return client.get(list_url)

    @pytest.fixture()
    def detail_response(self, client, detail_url, section, lectures, lecture_comments):
        return client.get(detail_url)


@pytest.mark.django_db(transaction=True)
class TestLectureAPIs(Base):

    def test_lecture_list(self, list_response, lectures, section):

        assert list_response.status_code == 200
        assert len(list_response.data) == len(lectures)
        assert list_response.data[0].get('duration') == lectures[0].duration

    def test_lecture_detail(self, detail_response, lectures, lecture_comments):

        assert detail_response.status_code == 200
        assert detail_response.data.get('id') == lectures[0].id
        assert detail_response.data.get('next_lecture') == lectures[0].next_lecture
        assert detail_response.data.get('get_all_comments')[0].get('id') == lecture_comments[0].id
        assert detail_response.data.get('get_all_comments')[0].get('user') == lecture_comments[0].user.id
