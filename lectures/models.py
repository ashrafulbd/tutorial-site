from django.db import models

# Create your models here.
from lectures.constant import DEFAULT_LECTURE_HTML_TEXT
from users.models import User


class Image(models.Model):
    image = models.ImageField(upload_to="image/%Y/%m/%d")

    # https://stackoverflow.com/questions/24373341/django-image-resizing-and-convert-before-upload

    def __str__(self):
        return f'{self.id}'


class Lecture(models.Model):
    createdAt = models.DateTimeField(auto_now_add=True)

    lecture_serial = models.PositiveIntegerField(blank=False, null=False)
    title = models.CharField(max_length=200, blank=False, null=False)
    description = models.TextField(blank=False, null=False, default=DEFAULT_LECTURE_HTML_TEXT)
    images = models.ManyToManyField(Image, blank=True)
    duration = models.PositiveSmallIntegerField(default=2)

    @property
    def next_lecture(self):
        parent_section = self.followed_section.all()
        if parent_section:
            next_lecture_list = parent_section[0].lectures. \
                filter(lecture_serial__gt=self.lecture_serial).order_by('lecture_serial')

            if next_lecture_list:
                return f'{parent_section[0].id}/{next_lecture_list.first().id}'
            return None
        return None

    @property
    def previous_lecture(self):
        parent_section = self.followed_section.all()
        if parent_section:
            next_lecture_list = parent_section[0].lectures. \
                filter(lecture_serial__lt=self.lecture_serial).order_by('lecture_serial')

            if next_lecture_list:
                return f'{parent_section[0].id}/{next_lecture_list.last().id}'
            return None
        return None

    @property
    def get_all_comments(self):
        return LectureComment.objects.filter(lecture=self.id)

    def __str__(self):
        return f'{self.lecture_serial}: {self.title}'


class LectureComment(models.Model):
    createdAt = models.DateTimeField(auto_now_add=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    lecture = models.ForeignKey(Lecture, on_delete=models.CASCADE)
    comment = models.TextField(blank=False, null=False)

    def __str__(self):
        return f'{self.user.email} > {self.comment} > {self.lecture.title}'
