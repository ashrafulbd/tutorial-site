from django.contrib import admin

# Register your models here.
from lectures.models import Lecture, Image, LectureComment

admin.site.register(Lecture)
admin.site.register(LectureComment)
admin.site.register(Image)
